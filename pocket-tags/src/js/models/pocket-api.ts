import { SetOps, querystring, eif, def } from "../utils";
import { MyJsonConverter, JsonConverter } from "./bookmark-converter";
import { TypedEvent } from "../views/view";

export interface PocketApiSettings {
   consumerKey: string;
   redirectUrl: string;
}

export class ApiFactory {
   static pocketDs() {
      return new PocketDataSource({
         redirectUrl: window.location.href.replace(/^http:/i, "https:"),
         consumerKey: "ODQyMzktNzY1ZGVkMTQwZTE1MWEzZjAzMTA1YWJl"
      });
   }

   static sampleDs() {
      return new SampleDataSource({
         file: "sample.json",
         redirectUrl: "",
         consumerKey: ""
      });
   }

   static async myjsonDs(key: string) {
      const sl = await new MyJsonConverter().import(key);
      const results = PocketRetrievalResults.fromMetadata(sl.bookmarks);
      return new ResultDataSource(results, sl.creator);
   }

   static jsonDs(json: string) {
      const sl = new JsonConverter().import(json);
      const results = PocketRetrievalResults.fromMetadata(sl.bookmarks);
      return new ResultDataSource(results, sl.creator);
   }

   static async createDataSource(params: {
      mode?: "myjson" | "sample" | "pocket";
      data?: string;
      key?: string;
   }): Promise<ApiDataSource> {
      // Override querystring with passed parameters
      params = $.extend(querystring(), params);
      switch (params["mode"]) {
         case "myjson":
            return await this.myjsonDs(params["key"]);
         case "sample":
            return this.sampleDs();
         default:
            return this.pocketDs();
      }
   }

   static async createApi(datasource: ApiDataSource) {
      await datasource.authorize();
      return new PocketApi(datasource);
   }
}

export interface IRetrieveEventArgs {
   results: PocketRetrievalResults;
   params: IRetrieveParameters;
}

export interface IActionEventArgs {
   actions: IActionParameters[];
}

export abstract class ApiDataSource {
   /* OVERWRITE THESE */

   protected _username: string;
   abstract get connected(): boolean;
   abstract get readonly(): boolean;
   abstract async retrieve(
      params: IRetrieveParameters
   ): Promise<PocketRetrievalResults>;
   abstract async action(actions: IActionParameters[]): Promise<void>;

   async authorize(): Promise<void> {
      return await $.when();
   }

   /* OVERWRITE THESE */

   get username() {
      return this._username;
   }

   protected mapObjProperties(
      src: Object,
      shouldAdd: boolean | ((key: string, value: any) => boolean),
      map: (key: string, value: any) => any
   ) {
      const shouldAddFn =
         typeof shouldAdd === "boolean" ? () => shouldAdd : shouldAdd;
      const target = {};
      $.each(src, (k, v) => {
         if (shouldAddFn(k, v)) {
            target[k] = map(k, v);
         }
      });
      return target;
   }

   protected mapResults(response: {
      [key: string]: any;
   }): PocketRetrievalResults {
      const includedFields: any = {
         authors: (authors: any) => $.map(authors, v => v.name),
         tags: (tags: { [key: string]: any }) => Object.keys(tags),
         image: (i: any) => (i || {}).src,
         excerpt: true,
         favorite: true,
         given_title: true,
         resolved_url: true,
         resolved_title: true,
         sort_id: true,
         status: true,
         item_id: true,
         top_image_url: true,
         time_added: true
      };

      // Conditionally filters and/or maps one object's properties into a new object
      const fieldMapper = this.mapObjProperties.bind(this);

      function mapItem(key: string, value: any) {
         const shouldIncludeField = (k: string, _: any) =>
            includedFields.hasOwnProperty(k);

         const mapField = (k: string, v: any) => {
            return $.isFunction(includedFields[k]) ? includedFields[k](v) : v;
         };

         value = $.extend(
            {
               tags: [],
               authors: [],
               image: "",
               top_image_url: ""
            },
            value
         );
         return fieldMapper(value, shouldIncludeField, mapField);
      }

      response = this.mapObjProperties(response.list, true, mapItem);
      return new PocketRetrievalResults(response);
   }
}

export class PocketApi {
   constructor(private _dataSource: ApiDataSource) {
      // Use with memory flag to get initial update.
      this.dataSourceChangedEvent.trigger();
   }

   get dataSource() {
      return this._dataSource;
   }
   set dataSource(value: ApiDataSource) {
      this._dataSource = value;
      this.dataSourceChangedEvent.trigger();
   }

   get readonly() {
      return this.dataSource.readonly;
   }

   get username() {
      return this.dataSource.username;
   }

   clone() {
      // Assumes an authorized datasource
      return new PocketApi(this.dataSource);
   }

   destroy() {}

   readonly dataSourceChangedEvent = new TypedEvent<PocketApi, void>(this);
   readonly retrieveEvent = new TypedEvent<PocketApi, IRetrieveEventArgs>(this);
   readonly actionEvent = new TypedEvent<PocketApi, IActionEventArgs>(this);

   /**
    * Retrieves a list of user data (https://getpocket.com/developer/docs/v3/retrieve).
    * @param {} params Optional parameters listed in API documentation.
    */
   async retrieve(
      params: IRetrieveParameters
   ): Promise<PocketRetrievalResults> {
      const results = await this.dataSource.retrieve(params);

      this.retrieveEvent.trigger({ params, results });
      return results;
   }

   /**
    * Adds, modifies, or removes a bookmark (https://getpocket.com/developer/docs/v3/modify).
    * @param actions
    */
   async action(actions: IActionParameters[]): Promise<void> {
      await this.dataSource.action(actions);
      this.actionEvent.trigger({ actions });
   }

   createEditor() {
      return new PocketLinkEditor(this);
   }
}

export class PocketDataSource extends ApiDataSource {
   public readonly readonly = false;
   private apiUrl = "https://getpocket.com/v3/";
   private preventRetry = false;
   constructor(public settings: PocketApiSettings) {
      super();

      if (/:\/\/localhost|:\/\/127.0.0.1/i.test(window.location.href)) {
         throw new Error(
            "The Pocket API authentication doesn't work with localhost."
         );
      }
   }

   async authorize() {
      try {
         if (this.connected) {
            return;
         } else if (this.requestToken) {
            await this.exchangeRequestTokenForAccessToken();
         } else {
            this.requestToken = await this.getRequestToken();
            this.promptUserForPermission();
         }
      } catch (err) {
         console.log(err);
         alert("There was an API connection or authentication error.");
         throw err;
      }
   }

   get connected() {
      return !!this.accessToken;
   }

   reset() {
      this.accessToken = null;
   }

   get requestToken() {
      return localStorage.getItem("pocket_request_token");
   }

   set requestToken(token) {
      localStorage.setItem("pocket_request_token", token);
   }

   get accessToken() {
      return localStorage.getItem("pocket_access_token");
   }

   set accessToken(token) {
      this.requestToken = null;
      localStorage.setItem("pocket_access_token", token);
   }

   private async callPocket(page: string, data: Object) {
      const accessToken = this.accessToken;
      $.extend(
         data,
         { consumer_key: btoa(this.settings.consumerKey) },
         accessToken ? { access_token: accessToken } : {}
      );

      try {
         return await $.post({
            url: this.apiUrl + page,
            data: data,
            dataType: "json",
            headers: {
               "X-Accept": "application/json"
            }
         });
      } catch (xhr) {
         console.error(xhr);
         if (!this.preventRetry) {
            this.preventRetry = true;
            this.reset();
         }
      }
   }

   /**
    * Obtain an API request token from Pocket.
    * (Step 2 from https://getpocket.com/developer/docs/authentication).
    */
   async getRequestToken() {
      const response = await this.callPocket("oauth/request", {
         redirect_uri: this.settings.redirectUrl
      });
      return response.code;
   }

   /**
    * Send the user to Pocket for manual authorization.
    * (Step 3 from https://getpocket.com/developer/docs/authentication).
    */
   promptUserForPermission() {
      window.location.href = `https://getpocket.com/auth/authorize?request_token=${
         this.requestToken
      }&redirect_uri=${this.settings.redirectUrl}`;
   }

   /**
    * Converts the API request token to an access token
    * after the user has authorized the app with Pocket.
    * (Step 5 from https://getpocket.com/developer/docs/authentication)
    */
   async exchangeRequestTokenForAccessToken() {
      if (this.accessToken) {
         // Use the existing access token.
         return $.when();
      }

      if (!this.requestToken) {
         // The request token was never accepted.
         // TODO: move index.html to setting
         window.location.href = "index.html";
         return;
      }

      const response = await this.callPocket("oauth/authorize", {
         code: this.requestToken
      });
      this._username = response.username;
      this.accessToken = response.access_token;
   }

   /**
    * Retrieves a list of user data (https://getpocket.com/developer/docs/v3/retrieve)
    * @param {} params Optional parameters listed in API documentation.
    */
   async retrieve(params: IRetrieveParameters) {
      console.log(params);

      // Trim empty properties
      params = this.mapObjProperties(
         params,
         (_, v) => v === 0 || !v,
         (_, v) => v
      );
      let results = await this.callPocket("get", params);
      return this.mapResults(results);
   }

   async action(actions: IActionParameters[]) {
      console.log(actions);
      if (!this.connected) {
         throw new Error("The user is not authorized to make this API call.");
      }
      actions = Array.isArray(actions) ? actions : [actions];
      await this.callPocket("send", { actions: actions });
   }
}

/**
 * Creates a datasource based on a saved response directly from the Pocket api
 */
export class SampleDataSource extends ApiDataSource {
   public readonly connected = true;
   public readonly readonly = true;

   constructor(public settings: DebugPocketApiSettings) {
      super();
      this._username = "Sample User";
   }

   async retrieve(params: IRetrieveParameters) {
      //const results = await import("../../sample.json");

      let results = await $.getJSON(this.settings.file);
      return this.mapResults(results);
   }

   async action(actions: IActionParameters[]) {
      console.log(actions);
      throw new Error("This should never be called, as it's read only.");
   }
}

/**
 * A readonly data source using parsed link results
 */
export class ResultDataSource extends ApiDataSource {
   public readonly readonly = true;
   public readonly connected = true;

   constructor(
      private results: PocketRetrievalResults,
      username: string = "Remote Data"
   ) {
      super();
      this._username = username;
   }

   async retrieve(params: IRetrieveParameters) {
      return this.results;
   }

   async action(actions: IActionParameters[]) {
      console.log(actions);
      throw new Error("This should never be called, as it's read only.");
   }
}

export class PocketLinkEditor {
   private parameters: IActionParameters[] = [];
   private complete: (() => void)[] = [];

   constructor(private api: PocketApi) {}

   async execute() {
      if (this.parameters.length) {
         await this.api.action(this.parameters);
      }
      this.complete.forEach(fn => fn());

      this.parameters = [];
      this.complete = [];
   }

   private enqueueCall(
      item_id: string,
      action: string,
      data: any = {},
      onAction: false | (() => void) = false
   ) {
      this.parameters.push({ item_id, action, ...data });
      onAction && this.complete.push(onAction);
   }

   setArchive(data: ILinkData, value: boolean) {
      // WARNING: There's a third to delete state unaccounted for.
      this.enqueueCall(data.item_id, value ? "archive" : "readd", {}, () => {
         data.status = value ? "1" : "0";
      });
      return this;
   }

   delete(item_id: string) {
      this.enqueueCall(item_id, "delete");
      return this;
   }

   setFavorite(data: ILinkData, value: boolean) {
      this.enqueueCall(
         data.item_id,
         value ? "favorite" : "unfavorite",
         {},
         () => (data.favorite = value ? "1" : "0")
      );
      return this;
   }

   setTags(data: ILinkData, tags: string[]) {
      this.enqueueCall(
         data.item_id,
         "tags_replace",
         {
            tags: tags.join()
         },
         () => (data.tags = tags)
      );
      return this;
   }

   addTags(data: ILinkData, tags: string[]) {
      this.enqueueCall(
         data.item_id,
         "tags_add",
         {
            tags: tags.join()
         },
         () => {
            data.tags = [...SetOps.union(new Set(data.tags), new Set(tags))];
            data.tags.sort();
         }
      );
      return this;
   }

   removeTags(data: ILinkData, tags: string[]) {
      this.enqueueCall(
         data.item_id,
         "tags_add",
         {
            tags: tags.join()
         },
         () => {
            data.tags = [
               ...SetOps.difference(new Set(data.tags), new Set(tags))
            ];
            data.tags.sort();
         }
      );
      return this;
   }

   add(data: ILinkMetadata) {
      this.enqueueCall(null, "add", {
         tags: data.tags.join() || "",
         title: data.given_title,
         url: data.resolved_url,
         time: data.time_added
      });
   }

   async importFromMetadata(metadata: ILinkMetadata[], chunkSize = 50) {
      let counter = 0;
      const links = [...metadata];

      while (links.length) {
         const link = links.pop();
         this.add(link);

         if (!links.length || ++counter % chunkSize === 0) {
            await this.execute();
         }
      }
   }
}

/**
 * The most basic information needed per bookmark for serialization.
 */
export interface ILinkMetadata {
   tags?: string[];
   resolved_url?: string;
   given_title?: string;
   time_added?: number;
}

export interface ILinkData extends ILinkMetadata {
   [key: string]: any;
   item_id?: string;
   authors?: string[];
   image?: string;
   exerpt?: string;
   favorite?: string;
   resolved_title?: string;
   sort_id?: string;
   status?: string;
   top_image_url?: string;
}

export interface DebugPocketApiSettings extends PocketApiSettings {
   file: string;
}

export interface IRetrieveParameters {
   /** unread, archive, all */
   state?: string;
   /** 0,1 */
   favorite?: string;
   tag?: string;
   /** article, video, image */
   contentType?: string;
   /** newest, oldest, tile, site */
   sort?: string;
   /** simple, complete */
   detailType?: string;
   search?: string;
   domain?: string;
   since?: number;
   count?: number;
   offset?: number;
}

export interface IActionParameters {
   action: string;
   tags?: string;
   time?: number;
   title?: string;
   url?: string;
   item_id: string;
   old_tag?: string;
   new_tag?: string;
}

export class PocketRetrievalResults {
   public tags: { [tagName: string]: string[] };

   private _linkList: ILinkData[];
   private _tagList: string[];

   private static counter = 0;

   /**
    *
    * @param {Function} refreshResults
    */
   constructor(public items: { [item_id: string]: ILinkData }) {
      // Convert ensure required fields exist usable results
      $.each(items, (key: string, item: ILinkData) => {
         def(item, "tags", []);
         def(item, "item_id", key);
      });

      this.tags = this.findTags();
   }

   static nextId() {
      return "id_" + ++PocketRetrievalResults.counter;
   }

   static fromMetadata(links: ILinkMetadata[]) {
      const items: { [item_id: string]: ILinkData } = {};

      links.forEach(l => {
         const item_id = def(l, "item_id", () =>
            PocketRetrievalResults.nextId()
         );
         items[item_id] = l;
      });

      return new PocketRetrievalResults(items);
   }

   linkMetadata() {
      return this.links.map(
         l =>
            <ILinkMetadata>{
               given_title: l.given_title,
               resolved_url: l.resolved_url,
               tags: l.tags || [],
               time_added: l.time_added
            }
      );
   }

   /**
    * Returns a new result set containing only the items with the spcified ids.
    */
   filterIds(ids: string[] | Set<string>) {
      const newItems: { [key: string]: ILinkData } = {};
      for (let id of ids) {
         newItems[id] = this.items[id];
      }

      return new PocketRetrievalResults(newItems);
   }

   private findTags() {
      const tags: { [tagName: string]: string[] } = {};

      $.each(this.items, (item_id: string, item: ILinkData) => {
         item.tags.forEach(tagKey => {
            const list = def(tags, tagKey, []);
            list.push(item_id);
         });
      });
      return tags;
   }

   /**
    * Finds all the tags of the given itemIds
    * @param [string|object[]] items A list of either item_id strings, or item objects
    */
   tagsByIds(ids: string[] | Set<string>) {
      const allTags = new Set();
      for (let id of ids) {
         this.items[id].tags.forEach(t => allTags.add(t));
      }
      return allTags;
   }

   linksByTag(tag: string) {
      return new Set(
         this.tags.hasOwnProperty(tag)
            ? this.tags[tag].map(id => this.items[id])
            : []
      );
   }

   get links() {
      return (
         this._linkList ||
         (this._linkList = Object.keys(this.items).map(k => this.items[k]))
      );
   }

   get tagList() {
      return this._tagList || (this._tagList = Object.keys(this.tags).sort());
   }
}
