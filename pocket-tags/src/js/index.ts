import "../index.html";
import "../favicon.ico";
import "../css/main.css";
import "../css/tagify.css";
import "../css/open-iconic-bootstrap.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/js/dist/util";
import "bootstrap/js/dist/index";
import "bootstrap/js/dist/popover";
import "bootstrap/js/dist/modal";
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/toast";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/tab";
import "./utils";
import "babel-polyfill";

import { ApiFactory, ApiDataSource } from "./models/pocket-api";
import FilterModel from "./models/filter-model";
import { MainController } from "./controllers/main-controller";
import { toast } from "./utils";

$(async function() {
   const options: any = {};
   let datasource: ApiDataSource;

   try {
      datasource = await ApiFactory.createDataSource(options);
      await datasource.authorize();
   } catch (exc) {
      toast(exc.message, {
         type: "danger",
         title: "Error connecting to service"
      });

      toast("Using a sample data set instead.", {
         type: "info"
      });

      options.mode = "sample";
      datasource = await ApiFactory.createDataSource(options);
   }

   const api = await ApiFactory.createApi(datasource);
   const filterModel = new FilterModel(api, {});
   new MainController(api, filterModel);
});
