import ListingsCollectionView from "../views/listings-collection-view";
import { PocketDataSource, ILinkData, PocketApi } from "../models/pocket-api";
import FilterModel from "../models/filter-model";

export default class ResultsController {
   listingsCollectionView: ListingsCollectionView;

   constructor(
      public api: PocketApi,
      public filterModel: FilterModel,
      public settings: any
   ) {
      this.listingsCollectionView = new ListingsCollectionView(
         this,
         filterModel
      );
      this.listingsCollectionView.appendTo($("#view-container"));
   }

   async archive(link: ILinkData, status: boolean) {
      await this.api
         .createEditor()
         .setArchive(link, status)
         .execute();
      this.listingsCollectionView.refresh(link.item_id);
   }

   async favorite(link: ILinkData, status: boolean) {
      await this.api
         .createEditor()
         .setFavorite(link, status)
         .execute();
      this.listingsCollectionView.refresh(link.item_id);
   }

   async delete(link: ILinkData) {
      const itemId = link.item_id;
      await this.api
         .createEditor()
         .delete(itemId)
         .execute();
      this.listingsCollectionView.removeView(itemId);
   }

   async setTags(link: ILinkData, tags: string[]) {
      await this.api
         .createEditor()
         .setTags(link, tags)
         .execute();
      this.listingsCollectionView.refresh(link.item_id);
   }
}
